# Descot - Desktop Mascot

## TL;DR

- draws PNG images as mascots on your desktop, staying below other windows
- positioned relative to chosen screen corner, allows scaling and offset options in either direction
- multi-monitor compatible
    - reacts to monitor changes and realigns mascots accordingly
    - allows strict or optimistic monitor assignment
    - clips overflowing image portions so they don't bleed into other monitors
- can quickly be hidden by creating a special file (for SFW toggle purposes)

![Sample Screenshot](example.png)

(example screenshot showing 2 running mascots: a girl on the left and a Debian logo badge on the bottom right)

## Disclaimer

**Consider this experimental software!**  
This was tested on Debian and Ubuntu running Awesome WM or Openbox standalone and has not been extensively tested with other distros, compositors, window managers or desktop environments. If you run into a problem that is reproducible in a VM setup, feel free to create an Issue here on GitLab.

~~Batteries~~ Images not included.  
**Hint:** you can use the tag `transparent_png` on yande.re or other image boards and \*boorus to find suitable images. The duckduckgo.com image search also offers a filter option for transparent image types specifically, as well.

## Dependencies

    python-gi
    python-gtk2
    python-cairo

## Configuration

Per mascot, create a config file according to the following format:

    [mascot]
    image=images/mascot.png
    position=br
    xoffset=64
    yoffset=-120
    monitor=2?
    scale=0.8
    mirror=0

(the `[mascot]` header and all properties are mandatory)

### Properties

Property | Format | Description
--- | --- | ---
`image` | string | Path to the image file to be displayed. May be absolute or relative. Relative paths are resolved according to the Python interpreter's working directory! (usually the directory where you execute `python ...` from)
`position` | string | Specifies which screen corner serves as the origin for the image alignment. Possible values: `tl` (top left), `tr` (top right), `bl` (bottom left), `br` (bottom right).
`xoffset` | int | Horizontal offset in pixels from the screen edge. Supports negative values. If appending `%` to the value, calculated according to screen width.
`yoffset` | int | Vertical offset in pixels from the screen edge. Supports negative values. If appending `%` to the value, calculated according to screen height.
`monitor` | str | Monitor index of the desired monitor. If set to `-1`, the primary monitor is used. If `?` is appended to the value (e.g. `2?`), the primary monitor is used as a fallback if the specified monitor is not present (optimistic approach). Otherwise, the mascot will not be displayed if the monitor is not present (strict approach).
`scale` | float / str | Will down- or upscale the specified `image` accordingly if set to anything other than `1.0`. May also be specified as integer percentage of the screen height by appending `%`. The offsets are *not* affected by scaling and are absolute!
`mirror` | bool | Specifies if the image is to be mirrored left/right.


## Usage

- before first execution, make it executable

        chmod +x ./descot

- run by specifying a configuration file

        ./descot /path/to/config

- you may specify screen margins in case you want to prevent custom panels or docks from getting covered

        ./descot [--margin-left N] [--margin-right N] [--margin-top N] [--margin-bottom N] \
                 [--set-below] [--reload-conky] /path/to/config

    **NOTE:** margins do not shift the config's offsets but simply shrink the drawing area, offsets stay relative to the screen

- see `./descot -h` for a full list and description of available command arguments


### Using the SFW lock

The file path of the lock file is currently coded as `~/.sfw_lock` at the top of the Python script. Whenever this file is created and as long as this file exists, the mascots will hide.

- this can be simply triggered by

        touch $HOME/.sfw_lock

- to display the mascots again, delete the file

        unlink $HOME/.sfw_lock

You can bind the first command or a corresponding toggling script to a keyboard shortcut to quickly hide all running mascots. See the included `toggle_sfw.sh` script for reference.


### Hide shadow with compton

The mascots use the `descot` window role and maybe identified accordingly in the `compton.conf`:

    shadow-exclude = [
        ...
        "r:e:descot",
        ...
    ];


### Making it play nicely with Awesome WM

Add the following rule set to your Awesome WM config:

    {
        rule = { role = "descot" },
        -- enable click-through: this allows to reach desktop menus
        -- (and the like) by clicking the mascot
        callback = function(c)
            local cairo = require("lgi").cairo
            local img = cairo.ImageSurface(cairo.Format.A1, 0, 0)
            c.shape_input = img._native img:finish()
        end,
        properties = {
            floating = true,
            sticky = true,
            border_width = 0,
            focusable = false
        }
    },

**Note:** do not use the `--set-below` flag with Awesome WM in conjunction with `wibar.ontop = false`, otherwise the mascots will end up covering your wibars!

#### Tip: centered master stack

You can prevent clients in the master stack from expanding to the full screen width when there are no slave clients present, just like in the screenshot above. This allows you to enjoy your desktop mascots while having the master client(s) displayed centered on the screen. To achieve this, set the following parameter in your Awesome WM config:

    beautiful.master_fill_policy = "master_width_factor"

The width of the client(s) with this setting is determined by the value of `beautiful.master_width_factor`!
